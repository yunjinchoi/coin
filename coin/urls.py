from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include('coinapp.urls', namespace='coinapp')),
    url(r'^collector/', include('collector.urls', namespace='collector')),
]
