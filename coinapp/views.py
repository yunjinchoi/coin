from django.shortcuts import render
from .models import Exchange

def index(request):
    return render(request, 'coinapp/index.html')

def post_list(request, title):
    data = Exchange.objects.all()
    return render(request, 'coinapp/post_list.html', {'data': data})