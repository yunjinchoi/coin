from bs4 import BeautifulSoup
from selenium import webdriver
import time
from sqlalchemy import create_engine
import pymysql
import MySQLdb


def start():
    pymysql.install_as_MySQLdb()
    driver = webdriver.PhantomJS()
    driver.implicitly_wait(3)

    url = 'https://upbit.com/exchange?code=CRIX.UPBIT.KRW-BTC'

    driver.get(url)

    time.sleep(15)

    soup = BeautifulSoup(driver.page_source, 'html.parser')

    titles = []
    values = []
    for item in soup.select('tr'):
        for i in item.select('.tit a'):
            titles.append(i.text.strip())
        for i in item.select('.price'):
            values.append(i.text.strip())

    import pandas as pd

    df = pd.DataFrame()

    df['title'] = titles
    df['price'] = values
    df['exchange'] = 'upbit'

    driver.close()

    connect_str = "mysql://{USER}:{PASSWORD}@{HOST}/{DBNAME}?charset=utf8".format(
        USER = 'mster@web-test-db',
        PASSWORD = 'master123!@#',
        HOST = 'web-test-db.mysql.database.azure.com',
        DBNAME = 'test'
    )

    engine = create_engine(connect_str)


    try:
        pd.read_sql('DELETE FROM collector WHERE exchange="upbit"', con=engine)
    except:
        print('')

    df['price'] = df['price'].apply(lambda x: x.replace(',',''))

    df.to_sql('collector', con=engine, if_exists='append', index=False)
