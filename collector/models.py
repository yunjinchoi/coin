from django.db import models

class Exchange(models.Model):
    title = models.CharField(max_length=50)
    price = models.IntegerField()
    exchange = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'collector'
        ordering = ('-id',)
